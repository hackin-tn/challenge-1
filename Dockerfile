FROM php:7.3.33-apache
USER www-data
COPY ./app /var/www/html
COPY ./php.ini /usr/local/etc/php/php.ini
RUN mkdir -p /var/www/html/sessions
RUN mv /var/www/html/flag.txt /var/www/html/`openssl rand -hex 32`.txt