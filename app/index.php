<html>

<head>
    <title>L&φ</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="container">
        <h1>
            <?php
            if (isset($_GET['page'])) {
                $page = $_GET['page'];
            } else {
                $page = 'home.php';
            }
            echo ucfirst(rtrim($page, '.php'));
            ?>
        </h1>
        <ul>
            <li>
                <a href="?page=home.php">Home</a>
            </li>
            <li>
                <a href="?page=login.php">Login</a>
            </li>
            <li>
                <a href="?page=about-us.php">About Us</a>
            </li>
        </ul>
        <?php
        include(__DIR__ . '/pages/' . $page);
        ?>
    </div>
</body>

</html>