<?php
if (!isset($_SESSION['username']) && isset($_POST['username'])) {
    $_SESSION['username'] = $_POST['username'];
}
if (isset($_SESSION['username'])) {
?>
    <p>
        You are now logged in as <?php echo $_SESSION['username']; ?>.
    </p>
    <p><a class="logout" href="?page=logout.php">Logout</a></p>
<?php
} else {
?>
    <form method="post">
        <input type="text" name="username" placeholder="Username">
        <input type="submit" value="Login">
    </form>
<?php
}
?>