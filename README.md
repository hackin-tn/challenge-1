# PHP RCE Challenge

Your goal is to get a Remote Code Execution (RCE) on the server.

You can launch the code locally with:

```bash
echo 'HTN{XXXXXXXXXXXXXXXXXXXXXXXXX}' > app/flag.txt
docker build -t htn_chall_1 .
docker run -rm -p 8080:80 htn_chall_1
```

And then, go to http://localhost:8080/ to visit the website.

Good luck ! 😛
